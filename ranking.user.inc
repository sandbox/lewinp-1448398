<?php

/**
 * @file
 * Users ranking page callback.
 */

/**
 * Page callback: Displays the ranking table.
 *
 * Displays a ranking sortable ranking table containing the name of the
 * participant, how much points he have and how many times he valided
 * challenges.
 *
 * @return array
 *   A render array for a page containing the ranking table.
 *
 * @see easy_contest_menu()
 */
function easy_contest_global_ranking() {
  $header = array(
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Points'), 'field' => 'points', 'sort' => 'desc'),
    array('data' => t('Validations'), 'field' => 'validations'),
  );

  $query = db_select('users', 'u', array('target' => 'slave'))
        ->extend('PagerDefault')
        ->extend('TableSort');
  $query->leftJoin('easy_contest_validation', 'v', 'v.uid = u.uid');
  $query->leftJoin('easy_contest_challenges', 'c', 'v.challenge = c.id');

  $query->addExpression('COUNT(DISTINCT v.id)', 'validations');
  $query->addExpression('SUM(c.points)', 'points');
  $query
    ->fields('u', array('uid', 'name'))
    ->condition('u.uid', 0, '>');

  if (variable_get('ranking_exclude_new_participants', TRUE)) {
    $query->isNotNull('points');
  }

  $query
    ->groupBy('u.uid')
    ->orderByHeader($header);
  
  $result = $query->execute();

  $rows = array();
  foreach ($result as $user) {
    $user_link = l($user->name, 'user/' . $user->uid);
    $rows[] = array($user_link, $user->points, $user->validations); 
  }

  $build = array();

  $build['rank_list'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No challengers.'),
  );

  return $build;
}
