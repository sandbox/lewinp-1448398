<?php

/**
 * @file
 * Challenges details and validation forms. 
 */

/**
 * Page callback: Displays a list of challenges.
 *
 * This page callback displays a table of all challenges, with the number of
 * validations and points for each.
 *
 * @return array
 *   A render array for a page containing a list of challenges.
 * 
 * @see easy_contest_menu()
 */
function easy_contest_challenges_list() {
  $header = array(
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Points'), 'field' => 'points', 'sort' => 'asc'),
    array('data' => t('Validations'), 'field' => 'validations'),
  );

  $query = db_select('easy_contest_challenges', 'c', array('target' => 'slave'))
        ->extend('PagerDefault')
        ->extend('TableSort');
  $query->leftJoin('easy_contest_validation', 'v', 'v.challenge = c.id');

  $query->addExpression('COUNT(v.id)', 'validations');
  $query
    ->fields('c', array('id', 'name', 'points'))
    ->groupBy('c.id')
    ->orderByHeader($header);
  
  $result = $query->execute();

  $rows = array();
  foreach ($result as $challenge) {
    $challenge_details_path = 'challenges/' . $challenge->id . '/details';
    $rows[] = array(l($challenge->name, $challenge_details_path), $challenge->points, $challenge->validations); 
  }

  $build = array();
  
  $build['challenges_list'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No challenges available.'),
  );
  
  return $build;
}

/**
 * Page callback: Displays challenge details.
 *
 * Displays challenge's description and his validation form.
 *
 * @param int $challenge_id
 *   Unique identifier of the challenge which has to be displayed.
 *
 * @return array
 *   A render array for a page containing the challenge details.
 *
 * @see easy_contest_menu()
 */
function easy_contest_challenge_details($challenge_id) {
  global $user;

  $query = db_select('easy_contest_challenges', 'c');

  $query
    ->fields('c', array('name', 'description', 'points'))
    ->condition('c.id', $challenge_id, '=');
  
  $result = $query->execute();
  $challenge = $result->fetchObject();

  $build['content'] = array(
    '#type' => 'fieldset',
    '#title' => $challenge->name,
    'description' => array(
      '#type' => 'markup',
      '#markup' => '<h3>' . t('Challenge description :') . '</h3><p>' . $challenge->description . '</p>',
    ),
  );
  if ( $user->uid == 0 ) {
    drupal_set_message(t('You have to register in order to validate this challenge.'), 'info');
  }
  else {
    $build['content']['validation'] = drupal_get_form('easy_contest_validation_form', $challenge_id);
  }

  return $build;
}

/**
 * Form constructor for the challenge validation form.
 *
 * @param int $challenge_id
 *   Unique identifier of the challenge.
 *
 * @see easy_contest_validation_form_submit()
 * @ingroup forms
 */
function easy_contest_validation_form($form_state, &$form_state, $challenge_id) {
  $form['validation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Challenge validation'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['validation']['solution'] = array(
    '#type' => 'textfield',
    '#title' => t('Answer'),
    '#size' => 30,
    '#maxlength' => 64,
    '#description' => t('Please write the challenge solution.'),
  );
  $form['challenge_id'] = array('#type' => 'hidden', '#value' => $challenge_id);

  $form['validation']['submit'] = array('#type' => 'submit', '#value' => t('Valider'));

  return $form;
}

/**
 * Check if user have tried too recently.
 * 
 * @param int $challenge_id
 *   Unique identifier of the challenge.
 * 
 * @param int $uid
 *   User identifier.
 * 
 * @return
 *   TRUE if the user is allowed to validate the challenge, FALSE otherwise.
 */
function easy_contest_validation_check_attempts($challenge_id, $uid) {
  $query = db_select('easy_contest_attempts', 'a');

  $attempt_delay = variable_get('challenge_attempt_delay', 60);

  $query
    ->fields('a')
    ->condition('challenge', $challenge_id, '=')
    ->condition('uid', $uid, '=')
    ->condition('timestamp', time()-$attempt_delay, '>');
  
  $result = $query->countQuery()->execute();
  $count  = $result->fetchObject()->expression;

  $already_attempted = $count > 0;
  
  $allowed = $already_attempted === FALSE;

  if (!$allowed) {
    drupal_set_message(t('Your last attempt was too recent.'), 'error');
  }
  else {
    $record = array(
      'id' => NULL,
      'challenge' => $challenge_id,
      'uid' => $uid,
      'timestamp' => time()
    );

    drupal_write_record('easy_contest_attempts', $record);
  }

  return $allowed;
}

/**
 * Form submission handler for easy_contest_validation_form().
 */
function easy_contest_validation_form_submit($form, &$form_state) {
  global $user;

  if ( $user->uid == 0 ) {
    drupal_set_message(t('You have to register in order to validate this challenge.'), 'error');
    return;
  }
  
  $challenge_id = (int) $form_state['values']['challenge_id'];

  $allowed = easy_contest_validation_check_attempts($challenge_id, $user->uid);
  if (!$allowed) {
    return;
  }

  $query = db_select('easy_contest_challenges', 'c');

  $query->fields('c', array('id', 'name', 'points', 'solution'));
  $query->condition('id', $challenge_id, '=');

  $result = $query->execute();
  $challenge = $result->fetchObject();

  $form_answer = trim($form_state['values']['solution']);

  if ($form_answer ==  $challenge->solution) {
    $query = db_select('easy_contest_validation', 'v');
    $query->fields('v', array('id', 'challenge', 'uid', 'timestamp'));
    $query->condition('uid', $user->uid, '=');
    $query->condition('challenge', $challenge_id, '=');
    $query->addExpression('COUNT(v.id)', 'validations');

    $result = $query->execute();
    $challenge = $result->fetchObject();

    drupal_set_message(t('Good job ! Your solution is correct.'), 'status');

    if ($challenge->validations != 0) {
      drupal_set_message(t('You have already validated this challenge.'), 'warning');
    }
    else {

      $record = array(
        'id' => NULL,
        'challenge' => $challenge_id,
        'uid' => $user->uid,
        'timestamp' => time()
      );

      drupal_write_record('easy_contest_validation', $record);

      drupal_set_message(t('You have validated this challenge.'), 'status');
    }

  }
  else {
    drupal_set_message(t('Sorry, your answer is not correct.'), 'error');
  }
}
