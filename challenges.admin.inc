<?php

/**
 * @file
 * Challenge administration and module setting UI.
 */

/**
 * Page callback: Displays a list of challenges with action links.
 *
 * This page callback displays a table of all challenges, with the number of
 * validations and points for each. User can edit them or delete them.
 *
 * @return array
 *   A render array for a page containing a list of challenges.
 *
 * @see easy_contest_menu()
 */
function easy_contest_challenges_list() {
  $header = array(
    t('Name'),
    t('Points'),
    t('Validations'),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $rows = array();

  $query = db_select('easy_contest_challenges', 'c');
  $query->leftJoin('easy_contest_validation', 'v', 'v.challenge = c.id');

  $query->addExpression('COUNT(v.id)', 'validations');
  $query
    ->fields('c', array('id', 'name', 'points'))
    ->groupBy('c.id');
  
  $result = $query->execute();
  $challenges = $result->fetchAll();

  foreach ($challenges as $challenge) {
    $rows[] = array(
    check_plain($challenge->name),
    check_plain($challenge->points),
    check_plain($challenge->validations),
    l(t('Edit'), 'admin/config/contest/challenges/edit/' . $challenge->id),
    l(t('Delete'), 'admin/config/contest/challenges/delete/' . $challenge->id),
    );
  }

  if (!$rows) {
    $rows[] = array(array(
      'data' => t('No challenges available.'),
      'colspan' => 5,
    ));
  }

  $build['challenge_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $build;
}

/**
 * Form constructor for the challenge edit form.
 *
 * @param array $challenge
 *   The challenge which has to be edited.
 *
 * @see easy_contest_challenge_edit_form_submit()
 * @ingroup forms
 */
function easy_contest_challenge_edit_form($form, &$form_state, array $challenge = array()) {
  $challenge += array(
    'id' => NULL,
    'name' => '',
    'description' => '',
    'points' => 1,
    'created' => time(),
    'solution' => '',
  );

  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => 255,
    '#default_value' => $challenge['name'],
    '#description' => t('Challenge name.'),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $challenge['description'],
    '#description' => t('Challenge description.'),
    '#required' => TRUE,
  );
  $form['points'] = array(
    '#type' => 'textfield',
    '#maxlength' => 5,
    '#title' => t('Points'),
    '#default_value' => $challenge['points'],
    '#description' => t('How many points you get after validating this challenge.'),
    '#required' => TRUE,
  );
  $form['solution'] = array(
    '#type' => 'textfield',
    '#title' => t('Solution'),
    '#maxlength' => 255,
    '#default_value' => $challenge['solution'],
    '#description' => t('Challenge solution.'),
    '#required' => TRUE,
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $challenge['id'],
  );
  $form['created'] = array(
    '#type' => 'value',
    '#value' => $challenge['created'],
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form submission handler for easy_contest_challenge_edit_form().
 */
function easy_contest_challenge_edit_form_submit($form, &$form_state) {
  
  $record = $form_state['values'];

  if (empty($form_state['values']['id'])) {
    drupal_write_record('easy_contest_challenges', $record);
  }
  else {
    drupal_write_record('easy_contest_challenges', $record, 'id');
  }

  drupal_set_message(t('Challenge %challenge has been saved.', array('%challenge' => $form_state['values']['name'])));
  watchdog('easy_contest', 'Challenge %challenge has been saved.', array('%challenge' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('Edit'), 'admin/config/contest/challenges/edit/' . $form_state['values']['id']));
  $form_state['redirect'] = 'admin/config/contest/challenges';
}

/**
 * Form constructor for the challenge delete confirm form.
 *
 * @param array $challenge
 *   The challenge which has to be deleted.
 *
 * @see easy_contest_challenge_delete_form_submit()
 * @ingroup forms
 */
function easy_contest_challenge_delete_form($form, &$form_state, array $challenge) {
  $form['challenge'] = array(
    '#type' => 'value',
    '#value' => $challenge,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete %challenge?', array('%challenge' => $challenge['name'])),
    'admin/config/contest/challenges',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Form submission handler for easy_contest_challenge_delete_form().
 */
function easy_contest_challenge_delete_form_submit($form, &$form_state) {
  $challenge = $form['challenge']['#value'];

  db_delete('easy_contest_challenges')
    ->condition('id', $challenge['id'])
    ->execute();
  
  db_delete('easy_contest_validation')
    ->condition('challenge', $challenge['id'])
    ->execute();

  drupal_set_message(t('Challenge %name has been deleted.', array('%name' => $challenge['name'])));
  watchdog('challenges', 'Challenge %name has been deleted.', array('%name' => $challenge['name']), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/config/contest/challenges';
}

/**
 * Form constructor for the module settings form.
 *
 * @ingroup forms
 */
function easy_contest_settings_form($form, &$form_state) {
  $form['ranking_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ranking settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Ranking page settings.'),
  );
  $form['ranking_settings']['ranking_exclude_new_participants'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exlude new participants'),
    '#description' => t('Hide people who made 0 validations.'),
    '#default_value' => variable_get('ranking_exclude_new_participants', TRUE),
    '#maxlenght' => 10,
  );

  $form['cheating_prevention'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cheating prevention'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Cheating prevention configuration.'),
  );
  $form['cheating_prevention']['challenge_attempt_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Attempt delay in seconds'),
    '#default_value' => variable_get('challenge_attempt_delay', 60),
    '#maxlenght' => 10,
  );

  return system_settings_form($form);
}
